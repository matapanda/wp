<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpres' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DUDEPh@/dN$ HFs3|PK5Q,Dh[%:V@t9Gmx,g?=v]8k:06(z8T{LR4|Dlxk<TJ(vz' );
define( 'SECURE_AUTH_KEY',  '}B`gW3s:C(4IV7@8`ZlvEw7}X#Z}WlTGCz}$7o4IioI v/UFvQ# aFy7jS8D[J&l' );
define( 'LOGGED_IN_KEY',    'f*xwd3%Z]{<Oa0))-bapo2tjxct1lI-t2 ?4F`pUEGo,Wx[v=!ybRTpH:+A.DQz}' );
define( 'NONCE_KEY',        'a!>Aq[{BJY+xl,TR)v9QB*nBF9Pi2Y?>[ &7bSEY+a/4wY7E._!iUT-+HYiJ6*x^' );
define( 'AUTH_SALT',        ')tH$Pj6|uYwC`y<wdU=^/t/$C$t_G0`|Kl7kH`Xy:XjQI*+Q2y%uJ|jW2Asp.VHf' );
define( 'SECURE_AUTH_SALT', '9pB<O_+S|%mvn^#YgX+QM4Xqie.;U[-?nopY58`w{qb|zDv<n<at|/#T4)0M0qId' );
define( 'LOGGED_IN_SALT',   '9%e1,h6[XDZ;].qF+D4As2^imkaF=f3,{79o{^.N>?^k~GY+*Z1A/26H1%KoaRvP' );
define( 'NONCE_SALT',       '^8hPCr}ztA+jXt>K9+P:en]7o[d63B%`UOO((z:B7H*)O2Pz17`4qtE.wAH:Hg2t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
